package com.pure.pureiecho.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.pure.pureiecho.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class CheckboxOptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<String> strOptions = new ArrayList<>();
    CheckboxOptionSelection checkboxOptionSelection;
    String strSelectOption = "";
    ArrayList<String> SelectedOption =  new ArrayList<>();

    public CheckboxOptionAdapter(Context mContext, ArrayList<String> strOptions, String strSelectOption) {
        this.mContext = mContext;
        this.strOptions = strOptions;
        this.strSelectOption = strSelectOption;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_checkbox_option, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.mCkbOption.setText(strOptions.get(position));
            try {
                if(strSelectOption != null && !strSelectOption.equalsIgnoreCase("")) {
                    JSONArray jsonArraySelectOption = new JSONArray(strSelectOption);
                    if (jsonArraySelectOption != null) {
                        boolean isSelect = false;
                        for (int i = 0; i < jsonArraySelectOption.length(); i++) {
                            if (jsonArraySelectOption.get(i).toString().equalsIgnoreCase(strOptions.get(position))) {
                                isSelect = true;
                                SelectedOption.add(strOptions.get(position));
                                viewHolder.mCkbOption.setChecked(true);
                            } else {
                                if (!isSelect) {
                                    viewHolder.mCkbOption.setChecked(false);
                                }
                            }
                        }
                    }
                } else {
                    viewHolder.mCkbOption.setChecked(false);
                }

            } catch (JSONException | NullPointerException e) {
                e.printStackTrace();
            }

            viewHolder.mCkbOption.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    checkboxOptionSelection = (CheckboxOptionSelection)mContext;
                    if(isChecked){
                        viewHolder.mCkbOption.setChecked(true);
                        SelectedOption.add(strOptions.get(position));
                        checkboxOptionSelection.SelectedCheckboxOption(position,
                                SelectedOption);
                    }else {
                        viewHolder.mCkbOption.setChecked(false);
                        if(SelectedOption.contains(strOptions.get(position))){
                            SelectedOption.remove(strOptions.get(position));
                        }
                        checkboxOptionSelection.SelectedCheckboxOption(position,
                                SelectedOption);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return strOptions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox mCkbOption;

        public ViewHolder(View itemView) {
            super(itemView);
            mCkbOption = itemView.findViewById(R.id.ckbOption);
        }
    }

    public interface CheckboxOptionSelection{
        void SelectedCheckboxOption(int Position, ArrayList<String> SelectedOption);
    }
}
