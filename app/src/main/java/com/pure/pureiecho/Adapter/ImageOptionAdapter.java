package com.pure.pureiecho.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.pure.pureiecho.R;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class ImageOptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    String[] stringOptions;
    AnswerSelection answerSelection;
    String strSelectOption = "";
    int selectedPosition = -1;

    public ImageOptionAdapter(Context mContext, String[] stringOptions, String strSelectOption) {
        this.mContext = mContext;
        this.stringOptions = stringOptions;
        this.strSelectOption = strSelectOption;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_image_option_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            if (!strSelectOption.equalsIgnoreCase("")) {
                if (strSelectOption.equalsIgnoreCase(String.valueOf(position))) {
                    /*viewHolder.mImgSelect.setVisibility(View.VISIBLE);*/
                    viewHolder.mRbSelection.setChecked(true);
                }else {
                    /*viewHolder.mImgSelect.setVisibility(View.GONE);*/
                    viewHolder.mRbSelection.setChecked(false);
                }
            } else {
                if (selectedPosition == position) {
                    /*viewHolder.mImgSelect.setVisibility(View.VISIBLE);*/
                    viewHolder.mRbSelection.setChecked(true);
                } else {
                    /*viewHolder.mImgSelect.setVisibility(View.GONE);*/
                    viewHolder.mRbSelection.setChecked(false);
                }
            }

            //region Image
            String picUrl = null;
            try {
                URL urla = null;
                urla = new URL(stringOptions[position].replaceAll("%20", " "));
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.with(mContext)
                        .load(picUrl)
                        .into(viewHolder.mImgOption, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {

                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                   /* SendMail sm = new SendMail(mContext, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();*/
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                   /* SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();*/
            }
            //endregion

            viewHolder.mLlImgOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answerSelection = (AnswerSelection)mContext;
                    selectedPosition = position;
                    strSelectOption = String.valueOf(position);
                    notifyDataSetChanged();
                    answerSelection.SelectedAnswer(position, "1");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return stringOptions.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImgOption, mImgSelect;
        private RadioButton mRbSelection;
        private LinearLayout mLlImgOption;

        public ViewHolder(View itemView) {
            super(itemView);
            mImgOption = itemView.findViewById(R.id.imgOption);
            mImgSelect = itemView.findViewById(R.id.imgSelect);
            mRbSelection = itemView.findViewById(R.id.rbSelection);
            mLlImgOption = itemView.findViewById(R.id.llImgOption);
        }
    }

    public interface AnswerSelection{
        void SelectedAnswer(int Position, String Type);
    }
}
