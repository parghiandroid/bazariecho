package com.pure.pureiecho.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.pure.pureiecho.R;

import java.util.ArrayList;

public class RadioOptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<String> strOptions = new ArrayList<>();
    RadioOptionSelection radioOptionSelection;
    int selectedPosition = -1;
    String strSelectOption;

    public RadioOptionAdapter(Context mContext, ArrayList<String> strOptions, String strSelectOption) {
        this.mContext = mContext;
        this.strOptions = strOptions;
        this.strSelectOption = strSelectOption;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_radio_option, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.mRbOption.setText(strOptions.get(position));

            if (!strSelectOption.equalsIgnoreCase("")) {
                if (strSelectOption.equalsIgnoreCase(strOptions.get(position))) {
                    viewHolder.mRbOption.setChecked(true);
                }else {
                    viewHolder.mRbOption.setChecked(false);
                }
            } else {
                if (selectedPosition == position) {
                    viewHolder.mRbOption.setChecked(true);
                } else {
                    viewHolder.mRbOption.setChecked(false);
                }
            }

            viewHolder.mRbOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = position;
                    strSelectOption = strOptions.get(position);
                    notifyDataSetChanged();
                    radioOptionSelection = (RadioOptionSelection) mContext;
                    radioOptionSelection.SelectedRadioOption(position, strOptions.get(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return strOptions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RadioButton mRbOption;

        public ViewHolder(View itemView) {
            super(itemView);
            mRbOption = itemView.findViewById(R.id.rbOption);
        }
    }

    public interface RadioOptionSelection {
        void SelectedRadioOption(int Position, String Value);
    }
}
