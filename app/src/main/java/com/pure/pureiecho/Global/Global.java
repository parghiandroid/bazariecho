package com.pure.pureiecho.Global;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.pure.pureiecho.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class Global {
    private static int i = 0;

    public static AlertDialog internetAlert;

    //region Alertdailog
    public static void alertdailog(Context context) {
        new AlertDialog.Builder(context, R.style.MyDialogTheme)
                .setTitle("Network Error!")
                .setMessage("Please check your network!")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    //endregion

    // region Alertdailog
    public static void AlertDailog(Context context, String strInternet) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_layout_internet_dialog, null);
        final TextView txtNetworkError = (TextView) row.findViewById(R.id.txtNetworkError);
        final TextView txtCheckInternet = (TextView) row.findViewById(R.id.txtCheckInternet);
        final TextView txtOk = (TextView) row.findViewById(R.id.txtOk);

        txtOk.setTypeface(Typefaces.Snippet(context));
        txtNetworkError.setTypeface(Typefaces.Snippet(context));
        txtCheckInternet.setTypeface(Typefaces.Snippet(context));


        if (strInternet.equalsIgnoreCase("Not connected to Internet")) {
            if (i == 0) {
                i = 1;
                AlertDialog.Builder i_builder = new AlertDialog.Builder(context);
                internetAlert = i_builder.create();
                internetAlert.setCancelable(false);
                internetAlert.setView(row);
                txtOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        internetAlert.dismiss();
                        //FOR CLOSE APP...
                        System.exit(0);
                    }
                });
                internetAlert.show();
            }
        } else {
            i = 0;
            internetAlert.dismiss();
        }
    }
    //endregion

    public static void Toast(Context context, String string) {
        Toast.makeText(context, string, Toast.LENGTH_LONG).show();
    }

    //region Check Internet Connection
    @SuppressLint("MissingPermission")
    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo activeNetworkInfo = null;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    //endregion

    //region FOR CHANGE DATE FORMATE...
    public static String changeDateFormate(String dateString, String yourFormate, String requiredFormate) {
        SimpleDateFormat sdf = new SimpleDateFormat(yourFormate);//set format of date you receiving from db
        Date date = null;
        SimpleDateFormat newDate = null;
        try {
            date = (Date) sdf.parse(dateString);
            newDate = new SimpleDateFormat(requiredFormate, Locale.US);//set format of new date

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate.format(date);
    }
    //endregion

    //region For Generate HeaderParam
    public static HashMap<String, String> headers() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", StaticUtility.Content_Type);
        headers.put("app-id", StaticUtility.App_Id);
        headers.put("app-secret", StaticUtility.App_Secret);
        return headers;
    }
    //endregion

    //region FOR GENERATE JSON OBJECT...
    public static JSONObject bodyParameter(String[] key, String[] val) {
        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < key.length; i++) {
            try {
                jsonObject.put(key[i], val[i]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }
    //endregion

    //region FOR CREATE STATIC QUERY STRING URL...
    public static String queryStringUrl(Context context) {
        String devicename = SharedPreference.GetPreference(context, StaticUtility.sDEVICEINFO,
                StaticUtility.sDEVICENAME);
        String deviceos = SharedPreference.GetPreference(context, StaticUtility.sDEVICEINFO,
                StaticUtility.sDEVICEOS);
        String app_version = SharedPreference.GetPreference(context, StaticUtility.sDEVICEINFO,
                StaticUtility.sAPPVERSION);
        devicename = devicename.replace(" ", "");
        return "?app_type=Android&app_version=" + app_version + "&device_name=" + devicename
                + "&system_version=" + deviceos;
    }
    //endregion

    //region FOR VALIDATE EMAIL_ADDRESS...
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    //endregion

    //region DeviceInfo
    public static void DeviceInfo(Context mContext) {
        if (SharedPreference.GetPreference(mContext, StaticUtility.sDEVICEINFO, StaticUtility.sDEVICENAME) == null) {
            String model = Build.MODEL;
            String os = Build.VERSION.RELEASE;
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = null;
            try {
                info = manager.getPackageInfo(mContext.getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(mContext, StaticUtility.TO_EMAIL, StaticUtility.SUBJECT, e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            assert info != null;
            String version = info.versionName;
            SharedPreference.CreatePreference(mContext, StaticUtility.sDEVICEINFO);
            SharedPreference.SavePreference(StaticUtility.sDEVICENAME, model);
            SharedPreference.SavePreference(StaticUtility.sDEVICEOS, os);
            SharedPreference.SavePreference(StaticUtility.sAPPVERSION, version);
        }
    }//endregion

    //region OpenSystemKeyBoard
    public static void OpenSystemKeyBoard(View view, Activity activity) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }
    //endregion

    //region Keyboard Hide
    public static void HideSystemKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    //endregion
}
