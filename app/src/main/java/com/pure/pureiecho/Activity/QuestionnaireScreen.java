package com.pure.pureiecho.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;

import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pure.pureiecho.Adapter.CheckboxOptionAdapter;
import com.pure.pureiecho.Adapter.ImageOptionAdapter;
import com.pure.pureiecho.Adapter.RadioOptionAdapter;
import com.pure.pureiecho.Global.BodyParam;
import com.pure.pureiecho.Global.Global;
import com.pure.pureiecho.Global.SharedPreference;
import com.pure.pureiecho.Global.StaticUtility;
import com.pure.pureiecho.Global.Typefaces;
import com.pure.pureiecho.Model.Question;
import com.pure.pureiecho.R;
import com.pure.pureiecho.Service.API;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;

public class QuestionnaireScreen extends AppCompatActivity implements View.OnClickListener,
        ImageOptionAdapter.AnswerSelection, RadioOptionAdapter.RadioOptionSelection,
        CheckboxOptionAdapter.CheckboxOptionSelection, API.JSONResponse {
    //Context
    private Context mContext = QuestionnaireScreen.this;
    private String TAG = QuestionnaireScreen.class.getSimpleName();

    //Event Bus (Internet)
    public EventBus eventBus = EventBus.getDefault();

    //widget
    private TextView mTxtQuesTitle, mTxtAnsCount, mTxtQuesCount, mTxtQues;
    private ProgressBar mPbQuestion;
    private LinearLayout mLlAnswer, mLlBottom;
    private LinearLayout mLlBack, mLlNext;
    private TextView mTxtBack, mTxtNext;
    int intTotalQues = 0;
    int intQuesCount = 1;
    int intQus = 0;

    //Toolbar
    private TextView mTxtTitle;
    private ImageView mImgBack, mImgLogout;

    ArrayList<Question> questions;
    String[] answers;

    private EditText mEdtText, mEdtMutiLine;
    private RadioGroup mRadiogroup;

    private static ArrayList<String> arrayListCheckbox;
    private static JSONArray jsonArrayCheckbox = new JSONArray();

    private String strOptionImageValue = "", strOptionRadioValue = "";

    //Loader
    private RelativeLayout mRlLoader;

    private String strSurveyID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_questionnaire_screen);
        eventBus.register(this);

        strSurveyID = getIntent().getStringExtra("Survey_id");

        initializeWidget();
        setTypeFaces();
        setWidgetOperations();
        mPbQuestion.setProgress(intQuesCount);
        if (Global.isNetworkAvailable(mContext)) {
            GetQuestionary();
        } else {
            Global.AlertDailog(mContext, "Not connected to Internet");
        }
    }

    //region initializeWidget
    private void initializeWidget() {
        mTxtQuesTitle = findViewById(R.id.txtQuesTitle);
        mTxtAnsCount = findViewById(R.id.txtAnsCount);
        mTxtQuesCount = findViewById(R.id.txtQuesCount);
        mPbQuestion = findViewById(R.id.pbQuestion);
        mTxtQues = findViewById(R.id.txtQues);
        mLlAnswer = findViewById(R.id.llAnswer);
        mLlBottom = findViewById(R.id.llBottom);
        mLlBack = findViewById(R.id.llBack);
        mTxtBack = findViewById(R.id.txtBack);
        mLlNext = findViewById(R.id.llNext);
        mTxtNext = findViewById(R.id.txtNext);

        //Toolbar
        mTxtTitle = findViewById(R.id.txtTitle);
        mImgBack = findViewById(R.id.imgBack);
        mImgLogout = findViewById(R.id.imgLogout);

        //Loader
        mRlLoader = findViewById(R.id.rlLoader);

    }//endregion

    //region setTypeFaces
    private void setTypeFaces() {
        mTxtQuesTitle.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mTxtAnsCount.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mTxtQuesCount.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mTxtBack.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mTxtNext.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mTxtTitle.setTypeface(Typefaces.Snippet(mContext));
    }//endregion

    //region setWidgetOperations
    private void setWidgetOperations() {
        mLlBack.setOnClickListener(this);
        mLlNext.setOnClickListener(this);
        mImgLogout.setOnClickListener(this);
        mImgBack.setOnClickListener(this);

    }//endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        Global.AlertDailog(mContext, event);
    }//endregion

    //region onClick
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llNext:
                if (mTxtNext.getText().toString().equalsIgnoreCase("Next")) {
                    if (Validate()) {
                        if (intQuesCount < intTotalQues) {
                            intQuesCount++;
                            intQus++;
                            NextQues(intQuesCount);
                            mLlBack.setVisibility(View.VISIBLE);
                            if (intQuesCount == intTotalQues) {
                                mTxtNext.setText(getString(R.string.submit));
                            } else {
                                mTxtNext.setText(getString(R.string.next));
                            }
                        }
                    }
                } else {
                    if (Validate()) {
                        if (Global.isNetworkAvailable(mContext)) {
                            SaveSurvey();
                        } else {
                            Global.AlertDailog(mContext, "Not connected to Internet");
                        }
                    }
                }
                break;

            case R.id.llBack:
                if (intQuesCount > 1) {
                    intQuesCount--;
                    intQus--;
                    if (intQuesCount == 1) {
                        mLlBack.setVisibility(View.GONE);
                    }
                    PreviousQues(intQuesCount);
                    if (intQuesCount == intTotalQues) {
                        mTxtNext.setText(getString(R.string.submit));
                    } else {
                        mTxtNext.setText(getString(R.string.next));
                    }
                }
                break;

            case R.id.imgLogout:
                new AlertDialog.Builder(mContext)
                        .setMessage(R.string.logout_message)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreference.ClearPreference(mContext, StaticUtility.LOGIN_PREFERENCE);
                                Intent intent = new Intent(mContext, LoginScreen.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                        Intent.FLAG_ACTIVITY_NEW_TASK |
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                break;
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }//endregion

    //region NextQues
    private void NextQues(int Count) {
        mPbQuestion.setProgress(Count);
        mTxtAnsCount.setText(String.valueOf(intQuesCount) + " / ");
        mLlAnswer.removeAllViews();
        setQuestions();
    }//endregion

    //region PreviousQues
    private void PreviousQues(int Count) {
        mPbQuestion.setProgress(Count);
        mTxtAnsCount.setText(String.valueOf(intQuesCount) + " / ");
        mLlAnswer.removeAllViews();
        setQuestions();
    }//endregion

    //region Get Questionary List
    private void GetQuestionary() {
        mRlLoader.setVisibility(View.VISIBLE);
        String[] key = {BodyParam.pSERVEY_ID};
        String[] val = {strSurveyID};
        API.APICalling(mContext, null, TAG, "questionary", StaticUtility.GETQUESTIONS, key, val, Global.headers());
    }//endregion

    @Override
    public void JsonAPIResponse(String strAPIType, JSONObject response) {
        String strCode = response.optString("code");
        String strStatus = response.optString("status");
        String strMessage = response.optString("message");
        if (strCode.equals("200")) {
            if (strAPIType.equalsIgnoreCase("questionary")) {
                try {
                    mRlLoader.setVisibility(View.GONE);
                    JSONArray jsonArrayPayload = response.optJSONArray("payload");
                    if (jsonArrayPayload.length() > 0) {
                        questions = new ArrayList<>();
                        for (int i = 0; i < jsonArrayPayload.length(); i++) {
                            JSONObject jsonObjectPayload = (JSONObject) jsonArrayPayload.get(i);
                            String strQuestionId = jsonObjectPayload.optString("question_id");
                            String strQuestion = jsonObjectPayload.optString("question");
                            String strAnsType = jsonObjectPayload.optString("ans_type");
                            String strSortOrder = jsonObjectPayload.optString("sort_order");
                            String strIsMandatory = jsonObjectPayload.optString("is_mandatory");
                            String strAnsDisplayType = jsonObjectPayload.optString("ans_display_type");
                            Object objectAnsValue = jsonObjectPayload.get("ans_values");
                            if (!objectAnsValue.equals("")) {
                                JSONArray jsonArrayAnsValue = jsonObjectPayload.optJSONArray("ans_values");
                                ArrayList<String> strAnsValue = new ArrayList<>();
                                for (int j = 0; j < jsonArrayAnsValue.length(); j++) {
                                    strAnsValue.add(String.valueOf(jsonArrayAnsValue.get(j)));
                                }
                                questions.add(new Question(strQuestionId, strQuestion, strIsMandatory, strAnsDisplayType, strAnsValue));
                            } else {
                                questions.add(new Question(strQuestionId, strQuestion, strIsMandatory, strAnsDisplayType));
                            }
                        }
                        answers = new String[questions.size()];
                        intTotalQues = questions.size();
                        mPbQuestion.setMax(intTotalQues);
                        mTxtQuesCount.setText(String.valueOf(intTotalQues));
                        mTxtAnsCount.setText(String.valueOf(intQuesCount) + " / ");
                        setQuestions();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (strAPIType.equalsIgnoreCase("savesurvey")) {
                Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, ThankYouScreen.class);
                startActivity(intent);
                finish();
            }
        } else {
            mRlLoader.setVisibility(View.GONE);
            Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
        }
    }

    //region Save Survey List
    private void SaveSurvey() {
        mRlLoader.setVisibility(View.VISIBLE);
        String strClientId = SharedPreference.GetPreference(mContext, StaticUtility.LOGIN_PREFERENCE, StaticUtility.sCLIENT_ID);
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArrayQuestions = new JSONArray();
            for (int i = 0; i < questions.size(); i++) {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("question", questions.get(i).getQuestion());
                    if (questions.get(i).getAns_display_type().equalsIgnoreCase("checkbox")) {
                        JSONArray jsonArray = new JSONArray(answers[i]);
                        obj.put("answer", jsonArray);
                    } else {
                        obj.put("answer", answers[i]);
                    }
                    /*obj.put("answer", answers1.get(i));*/
                    jsonArrayQuestions.put(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            jsonObject.put(BodyParam.pSERVEY_ID, strSurveyID);
            jsonObject.put(BodyParam.pCLIENT_ID, strClientId);
            jsonObject.put(BodyParam.pQUESTIONS, jsonArrayQuestions);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        API.APICallingJson(mContext, null, TAG, "savesurvey",
                StaticUtility.SUBMITSURVEY, jsonObject, Global.headers());
    }//endregion

    //region set Questions
    private void setQuestions() {
        mTxtQues.setText(intQuesCount + " . " + questions.get(intQus).getQuestion());
        if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Text")) {
            createEditText();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Textarea")) {
            createMultiLineEditText();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Radio")) {
            createRadio();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Checkbox")) {
            createCheckBox();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("image")) {
            CreateImageViewRecyclerView();
        }
    }//endregion

    //region createMultiLineEditText
    @SuppressLint("NewApi")
    public void createMultiLineEditText() {
        mEdtMutiLine = new EditText(mContext);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10, 0, 10, 0);
        mEdtMutiLine.setLayoutParams(layoutParams);
        mEdtMutiLine.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        mEdtMutiLine.setSingleLine(false);
        mEdtMutiLine.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        mEdtMutiLine.setMinLines(4);
        mEdtMutiLine.setLines(4);
        mEdtMutiLine.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mEdtMutiLine.setTextSize(14);
        mEdtMutiLine.setGravity(Gravity.TOP);
        mEdtMutiLine.setPadding(10, 10, 10, 10);
        mEdtMutiLine.setTextColor(getResources().getColor(R.color.colorPrimary));
        mEdtMutiLine.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        mEdtMutiLine.setVerticalScrollBarEnabled(true);

        if (answers[intQus] != null) {
            if (!answers[intQus].equalsIgnoreCase("")) {
                mEdtMutiLine.setText(answers[intQus]);
            }
        }
        mLlAnswer.addView(mEdtMutiLine);

    }//endregion

    //region createEditText
    @SuppressLint("NewApi")
    public void createEditText() {
        mEdtText = new EditText(mContext);
        mEdtText.setInputType(InputType.TYPE_CLASS_TEXT);
        mEdtText.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mEdtText.setTextSize(14);
        mEdtText.setTextColor(getResources().getColor(R.color.colorPrimary));
        mEdtText.setPadding(10, 10, 10, 10);
        if (answers[intQus] != null) {
            if (!answers[intQus].equalsIgnoreCase("")) {
                mEdtText.setText(answers[intQus]);
            }
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10, 0, 10, 0);
        mEdtText.setLayoutParams(layoutParams);
        mLlAnswer.addView(mEdtText);
    }//endregion

    //region createRadio
    public void createRadio() {
        RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        RadioOptionAdapter radioOptionAdapter = null;
        if (questions.get(intQus).getOptions() != null && questions.get(intQus).getOptions().size() > 0) {
            if (answers[intQus] != null) {
                strOptionRadioValue = answers[intQus];
                radioOptionAdapter = new RadioOptionAdapter(mContext, questions.get(intQus).getOptions(), strOptionRadioValue);
            } else {
                strOptionRadioValue = "";
                radioOptionAdapter = new RadioOptionAdapter(mContext, questions.get(intQus).getOptions(), strOptionRadioValue);
            }
            recyclerView.setAdapter(radioOptionAdapter);
        }
        mLlAnswer.addView(recyclerView);
    }//endregion

    // region createCheckBox
    public void createCheckBox() {
        RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        CheckboxOptionAdapter checkboxOptionAdapter = null;
        if (questions.get(intQus).getOptions() != null && questions.get(intQus).getOptions().size() > 0) {
            if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
                try {
                    jsonArrayCheckbox = new JSONArray(answers[intQus]);
                    checkboxOptionAdapter = new CheckboxOptionAdapter(mContext, questions.get(intQus).getOptions(), answers[intQus]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                answers[intQus] = "";
                jsonArrayCheckbox = new JSONArray();
                checkboxOptionAdapter = new CheckboxOptionAdapter(mContext, questions.get(intQus).getOptions(), "");
            }
            recyclerView.setAdapter(checkboxOptionAdapter);
        }
        mLlAnswer.addView(recyclerView);

    }//endregion

    //region createLinearLayout
    public void createLinearLayoutHORIZONTAL(View view) {
        LinearLayout layout = new LinearLayout(mContext);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layout.setPadding(10, 10, 5, 5);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10, 0, 10, 0);
        layout.setLayoutParams(layoutParams);
        layout.setWeightSum(2f);
        layout.addView(view);
        mLlAnswer.addView(layout);
    }//endregion

    // region createLinearLayout
    public void createLinearLayoutVERTICAL(View view) {
        LinearLayout layout = new LinearLayout(mContext);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setPadding(10, 10, 5, 5);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                (0,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.weight = 1.0f;
        layoutParams.setMargins(10, 0, 10, 0);
        layout.setLayoutParams(layoutParams);
        layout.addView(view);
        createLinearLayoutHORIZONTAL(layout);
    }//endregion

    //region createImageView
    @SuppressLint("NewApi")
    public void createImageView() {
        String[] strings1 =
                {"https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/05/diamond-bracelet/1525167957_771145001509366348_cover_imageB-451 P_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/amour-bracelet/1525078305_723149001509364703_cover_imageB-451_P_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/elite-solitaire-bridal-ring/1525078058_757931001509360035_cover_imageDOC R-6 FW_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/dazzle-bridal-ring/1525076359_249097001509360481_cover_imageDOC%20R-6%20FY_1024x1024.jpg"};
        for (int i = 0; i < 2; i++) {
            LinearLayout layout = new LinearLayout(mContext);
            layout.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(10, 0, 10, 0);
            layout.setLayoutParams(layoutParams);
            layout.setWeightSum(2f);
            for (int j = 0; j < 2; j++) {
                LinearLayout layoutVertical = new LinearLayout(mContext);
                layoutVertical.setOrientation(LinearLayout.VERTICAL);
                LinearLayout.LayoutParams layoutVertiaclParams = new LinearLayout.LayoutParams
                        (LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);

                layoutVertiaclParams.weight = 1.0f;
                ImageView imageView = new ImageView(mContext);
                LinearLayout.LayoutParams imageLayoutParams = new LinearLayout.LayoutParams
                        (100, 100);
                imageLayoutParams.setMargins(10, 10, 10, 0);
                imageView.setLayoutParams(imageLayoutParams);
                /*imageView.setImageResource(R.drawable.ic_login_top_bg);*/

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(strings1[j].replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.with(mContext)
                            .load(picUrl)
                            .into(imageView, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {

                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                   /* SendMail sm = new SendMail(mContext, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();*/
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                   /* SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();*/
                }
                //endregion

                layoutVertical.addView(imageView);
                layout.addView(layoutVertical);
            }
            mLlAnswer.addView(layout);
        }

    }//endregion

    //region CreateImageViewRecyclerView
    public void CreateImageViewRecyclerView() {
        String[] strings =
                {"https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/05/diamond-bracelet/1525167957_771145001509366348_cover_imageB-451 P_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/amour-bracelet/1525078305_723149001509364703_cover_imageB-451_P_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/elite-solitaire-bridal-ring/1525078058_757931001509360035_cover_imageDOC R-6 FW_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/dazzle-bridal-ring/1525076359_249097001509360481_cover_imageDOC%20R-6%20FY_1024x1024.jpg"};
        RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        ImageOptionAdapter imageOptionAdapter = null;
        if (answers[intQus] != null) {
            strOptionImageValue = answers[intQus];
            imageOptionAdapter = new ImageOptionAdapter(mContext, strings, strOptionImageValue);
        } else {
            strOptionImageValue = "";
            imageOptionAdapter = new ImageOptionAdapter(mContext, strings, strOptionImageValue);
        }

       /* if (answers1.get(intQus) != null) {
            strOptionImageValue = answers1.get(intQus).toString();
            imageOptionAdapter = new ImageOptionAdapter(mContext, strings, strOptionImageValue);
        } else {
            strOptionImageValue = "";
            imageOptionAdapter = new ImageOptionAdapter(mContext, strings, strOptionImageValue);
        }*/
        recyclerView.setAdapter(imageOptionAdapter);
        mLlAnswer.addView(recyclerView);
    }//endregion

    //region Validate
    private boolean Validate() {
        if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("text")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (!TextUtils.isEmpty(mEdtText.getText().toString())) {
                    answers[intQus] = mEdtText.getText().toString();
                    return true;
                } else {
                    Toast.makeText(mContext, "Enter Answer", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (mEdtText.getText().length() > 0) {
                    answers[intQus] = mEdtText.getText().toString();
                } else {
                    answers[intQus] = "";
                }
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Textarea")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (mEdtMutiLine != null) {
                    if (!TextUtils.isEmpty(mEdtMutiLine.getText().toString())) {
                        answers[intQus] = mEdtMutiLine.getText().toString();
                        return true;
                    } else {
                        Toast.makeText(mContext, "Enter Answer", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                if (mEdtMutiLine.getText().length() > 0) {
                    answers[intQus] = mEdtMutiLine.getText().toString();
                } else {
                    answers[intQus] = "";
                }
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("radio")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (!strOptionRadioValue.equalsIgnoreCase("")) {
                    answers[intQus] = strOptionRadioValue;
                    return true;
                } else {
                    Toast.makeText(mContext, "Select Any Answer", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (!strOptionRadioValue.equalsIgnoreCase("")) {
                    answers[intQus] = strOptionRadioValue;
                } else {
                    answers[intQus] = "";
                }
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("checkbox")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (jsonArrayCheckbox != null && jsonArrayCheckbox.length() > 0) {
                    answers[intQus] = jsonArrayCheckbox.toString();
                    return true;
                } else {
                    answers[intQus] = "";
                    Toast.makeText(mContext, "Select Any Answer", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (jsonArrayCheckbox != null && jsonArrayCheckbox.length() > 0) {
                    answers[intQus] = jsonArrayCheckbox.toString();
                } else {
                    answers[intQus] = "";
                }
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("image")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (!strOptionImageValue.equalsIgnoreCase("")) {
                    answers[intQus] = strOptionImageValue;
                    return true;
                } else {
                    Toast.makeText(mContext, "Select Any Answer", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (!strOptionImageValue.equalsIgnoreCase("")) {
                    answers[intQus] = strOptionImageValue;
                } else {
                    answers[intQus] = "";
                }
                return true;
            }
        }
        return false;
    }//endregion

    @Override
    public void SelectedAnswer(int Position, String Type) {
        if (Type.equalsIgnoreCase("1")) {
            strOptionImageValue = String.valueOf(Position);
        } else {
            strOptionImageValue = "";
        }
    }

    @Override
    public void SelectedRadioOption(int Position, String Value) {
        strOptionRadioValue = Value;
    }

    @Override
    public void SelectedCheckboxOption(int Position, ArrayList<String> SelectedOptions) {
        arrayListCheckbox = SelectedOptions;
        if (SelectedOptions.size() > 0) {
            jsonArrayCheckbox = new JSONArray();
            for (int i = 0; i < SelectedOptions.size(); i++) {
                jsonArrayCheckbox.put(SelectedOptions.get(i));
            }
        } else {
            jsonArrayCheckbox = new JSONArray();
        }
    }
}
