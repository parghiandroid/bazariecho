package com.pure.pureiecho.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.pure.pureiecho.Global.SharedPreference;
import com.pure.pureiecho.Global.StaticUtility;
import com.pure.pureiecho.Global.Typefaces;
import com.pure.pureiecho.R;

public class ThankYouScreen extends AppCompatActivity {

    Context mContext = ThankYouScreen.this;
    private TextView mTxtThankyou;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you_screen);
        mTxtThankyou = findViewById(R.id.txtThankyou);
        mTxtThankyou.setTypeface(Typefaces.Snippet(mContext));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreference.ClearPreference(mContext, StaticUtility.LOGIN_PREFERENCE);
                Intent intent = new Intent(ThankYouScreen.this, LoginScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }, 5000);
    }
}
