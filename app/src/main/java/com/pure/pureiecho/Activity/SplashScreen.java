package com.pure.pureiecho.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.pure.pureiecho.Global.Global;
import com.pure.pureiecho.Global.SharedPreference;
import com.pure.pureiecho.Global.StaticUtility;
import com.pure.pureiecho.R;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;

public class SplashScreen extends AppCompatActivity {
    //Context
    Context mContext = SplashScreen.this;
    private String TAG = SplashScreen.class.getSimpleName();
    //Event Bus (Internet)
    public EventBus eventBus = EventBus.getDefault();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_splash_screen);
        eventBus.register(this);

        Global.DeviceInfo(mContext);
        if (Global.isNetworkAvailable(mContext)) {
            if (SharedPreference.GetPreference(mContext, StaticUtility.LOGIN_PREFERENCE,
                    StaticUtility.sCLIENT_ID) != null) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(SplashScreen.this, SurveyScreen.class);
                        startActivity(i);
                        finish();
                    }
                }, 5000);
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(SplashScreen.this, LoginScreen.class);
                        startActivity(i);
                        finish();
                    }
                }, 5000);
            }
        } else {
            Global.alertdailog(mContext);
        }
    }

    //region For EventBus onEvent
    public void onEvent(String event) {
        Global.alertdailog(mContext);
    }//endregion
}
