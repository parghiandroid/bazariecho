package com.pure.pureiecho.Model;

public class Survey {
    String survey_id;
    String title;

    public Survey(String survey_id, String title) {
        this.survey_id = survey_id;
        this.title = title;
    }

    public String getSurvey_id() {
        return survey_id;
    }

    public String getTitle() {
        return title;
    }
}
