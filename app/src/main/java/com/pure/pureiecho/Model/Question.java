package com.pure.pureiecho.Model;

import java.util.ArrayList;

public class Question {
    String question_id;
    String question;
    String is_mandatory;
    String ans_display_type;
    ArrayList<String> Options;

    public Question(String question_id, String question, String is_mandatory, String ans_display_type, ArrayList<String> options) {
        this.question_id = question_id;
        this.question = question;
        this.is_mandatory = is_mandatory;
        this.ans_display_type = ans_display_type;
        Options = options;
    }

    public Question(String question_id, String question, String is_mandatory, String ans_display_type) {
        this.question_id = question_id;
        this.question = question;
        this.is_mandatory = is_mandatory;
        this.ans_display_type = ans_display_type;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public String getQuestion() {
        return question;
    }

    public String getIs_mandatory() {
        return is_mandatory;
    }

    public String getAns_display_type() {
        return ans_display_type;
    }

    public ArrayList<String> getOptions() {
        return Options;
    }
}
